## INTRODUCTION

Module to generate sets of random word combinations, where you can combine words from two word sets to a unique combination. You will provide the words, separated by space, for each side of the combination.

You can have as many sets as you like, and for each set you can decide if the module should ensure that the generated combination is unique, or not. If you decide it to be unique, there will be added database records keeping track of the generated combinations, and you will not be able to generate more after you run out of possible combinations. This will add some overhead to the set. Admins can delete (flush) generated combinations.

You can also decide if you want to generate a token for the set, which will provide you with a token of the random combination you can use in other modules that accepts tokens.

### Use cases:

* You'd like a rememberable word combination to use as a random name for a group / content entity or similar
* You'd like a rememberable confirmation code
* You have some other reasons to combine words.

### Usage:

You would typically use it either through tokens, or through the service the module provides.

The service can be used like:

```php
\Drupal::service('random_word_combo.combiner')->getRandomCombo('machine_name_of_set', $dryRun = FALSE);
```

If you set `$dryRun` to `TRUE`, the combination will not be saved, even if ensure unique is enabled.

Alternatively, you can generate one manually through the UI.

## REQUIREMENTS

None, allthough token module is helpful.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
Manage the module under Structure -> Random Word Combos. Add new sets, or modify the default one.

## MAINTAINERS

Current maintainers for Drupal 10:

- Vegard A. Johansen (vegardjo) - https://www.drupal.org/u/vegardjo

