<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_ENTITY_TYPE_delete() for random_word_combo entities.
 *
 * Deletes the corresponding records from custom table when the entity itself is deleted.
 */
function random_word_combo_random_word_combo_delete(Drupal\random_word_combo\Entity\ComboSetConfig $entity) {
  $database = \Drupal::database();
  $setId = $entity->id();

  $database->delete('random_word_combo_combinations')
    ->condition('combo_set_id', $setId)
    ->execute();
}

/**
 * Implements hook_token_info().
 */
function random_word_combo_token_info() {
  $info = [];

  $info['types'] = [
    'random-word-combo' => [
      'name' => t('Random word combos'),
      'description' => t('Generate random word combos'),
    ],
  ];

  $info['tokens']['random-word-combo'] = [];

  $setsWithTokens = \Drupal::service('random_word_combo.combiner')->getSetsWithTokens();

  foreach ($setsWithTokens as $setId) {
    // Append a token definition for each set ID.
    $info['tokens']['random-word-combo'][$setId] = [
      'name' => t("Combo for $setId"),
      'description' => t('Generates a random word combo for @setId.', ['@setId' => $setId]),
    ];
  }

  return $info;
}


/**
 * Implements hook_tokens().
 */
function random_word_combo_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];

  if ($type == 'random-word-combo') {
    $setsWithTokens = \Drupal::service('random_word_combo.combiner')->getSetsWithTokens();

    foreach ($tokens as $name => $original) {
      foreach ($setsWithTokens as $setId) {
        // Match the token name to ensure it's for the current set ID.
        if (strpos($name, $setId) !== FALSE) {
          // Generate the random combo specifically for this set ID.
          $randomCombo = \Drupal::service('random_word_combo.combiner')->getRandomCombo($setId);
          $replacements[$original] = $randomCombo;
          break;
        }
      }
    }
  }

  return $replacements;
}

