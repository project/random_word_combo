<?php

namespace Drupal\random_word_combo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\random_word_combo\WordComboCombiner;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ComboSetConfigController extends ControllerBase {

  /**
   * The word combo combiner service.
   *
   * @var \Drupal\random_word_combo\WordComboCombiner
   */
  protected $combiner;

  public function __construct(WordComboCombiner $combiner) {
    $this->combiner = $combiner;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('random_word_combo.combiner')
    );
  }

  public function list() {
    $header = [
      $this->t('ID'),
      $this->t('Description'),
      $this->t('Operations'),
    ];

    $rows = [];

    // Load all ComboSetConfig entities.
    $combo_sets = \Drupal::entityTypeManager()->getStorage('random_word_combo')->loadMultiple();

    // Build the rows of the table.
    foreach ($combo_sets as $id => $combo_set) {
      $row = [];

      $row[] = [
        'data' => [
          '#type' => 'markup',
          '#markup' => '<strong>' . $this->t('@name', ['@name' => $combo_set->label()]) . '</strong><br />' .
                       '<em>Machine name: ' . $this->t('@id', ['@id' => $combo_set->id()]) . '</em>',
          '#allowed_tags' => ['strong', 'br', 'em'],
        ],
      ];

      $descriptionValues = $this->combiner->getDescriptionValues($combo_set->id());

      if ($combo_set->ensure_unique) {
        // @todo this needs to use t() + handle infintie possible combos if they are not saved.
        $descriptionMessage = 'Example combo: <strong>' . $descriptionValues['exampleCombo'] . '</strong><br />' .
                              'You have generated <strong>' . $descriptionValues['generatedCombos'] . '</strong> combinations ' .
                              'out of a possible <strong>' . $descriptionValues['possibleCombos'] . '</strong> combinations.';
      } else {
        $descriptionMessage = 'Example combo: <strong>' . $descriptionValues['exampleCombo'] . '</strong><br />' .
                              'There are <strong>' . $descriptionValues['possibleCombos'] . '</strong> possible combinations.<br />' .
                              'Unique combos not enforced, you can generate as many as you like.';
      }

      $row[] = [
        'data' => [
          '#type' => 'markup',
          '#markup' => $descriptionMessage,
          '#allowed_tags' => ['strong', 'br', 'em'],
        ],
      ];

      // Define the edit and delete operations.
      $operations = [
        'edit' => [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('entity.random_word_combo.edit_form', ['random_word_combo' => $id]),
        ],
        'generate' => [
          'title' => $this->t('Generate Combo'),
          'url' => Url::fromRoute('random_word_combo.generate_combo', ['random_word_combo' => $id]),
        ],
        'delete' => [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('entity.random_word_combo.delete_form', ['random_word_combo' => $id]),
        ],
      ];

      // Define the flush operation if relevant.
      if ($combo_set->ensure_unique) {
        $operations['flush'] = [
          'title' => $this->t('Flush Combos'),
          'url' => Url::fromRoute('random_word_combo.flush_combos', ['random_word_combo' => $id]),
        ];
      }

      // Add the operations dropdown to the row.
      $row[] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $operations,
        ],
      ];

      $rows[] = $row;
    }

    // Render the table.
    $build['combo_set_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No combo sets found.'),
    ];

    return $build;
  }

  public function flushCombos($random_word_combo) {
    $flushed = $this->combiner->flushCombosForSet($random_word_combo);

    if ($flushed) {
      $this->messenger()->addMessage($this->t('Combos for the set @id have been flushed.', ['@id' => $random_word_combo]));
    } else {
      $this->messenger()->addError($this->t('Failed to flush combos for the set @id.', ['@id' => $random_word_combo]));
    }

    return $this->redirect('random_word_combo.combo_set_config_list');
  }

  public function generateCombo($random_word_combo) {
    $combo = $this->combiner->getRandomCombo($random_word_combo, $dryRun = FALSE);

    if ($combo) {
      $this->messenger()->addMessage($this->t('Generated new combo: @combo', ['@combo' => $combo]));
    } else {
      $this->messenger()->addError($this->t('Failed to generate a new combo for the set @id.', ['@id' => $random_word_combo]));
    }

    return $this->redirect('random_word_combo.combo_set_config_list');
  }
}
