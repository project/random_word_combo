<?php

namespace Drupal\random_word_combo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Random Word Combo entity.
 *
 * @ConfigEntityType(
 *   id = "random_word_combo",
 *   label = @Translation("Random Word Combo"),
 *   admin_permission = "administer random word combos",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/random_word_combo/add",
 *     "edit-form" = "/admin/structure/random_word_combo/{random_word_combo}/edit",
 *     "delete-form" = "/admin/structure/random_word_combo/{random_word_combo}/delete",
 *     "collection" = "/admin/structure/random_word_combo"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\random_word_combo\Form\ComboSetConfigForm",
 *       "edit" = "Drupal\random_word_combo\Form\ComboSetConfigForm",
 *       "delete" = "Drupal\random_word_combo\Form\ComboSetConfigDeleteForm"
 *     }
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "left_words",
 *     "right_words",
 *     "ensure_unique",
 *     "generate_token",
 *   }
 * )
 */
class ComboSetConfig extends ConfigEntityBase {
  /**
   * The Random Word Combo ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Random Word Combo label.
   *
   * @var string
   */
  public $label;

  /**
   * Words on the left side.
   *
   * @var string
   */
  public $left_words;

  /**
   * Words on the right side.
   *
   * @var string
   */
  public $right_words;

  /**
   * Whether to ensure unique combinations.
   *
   * @var bool
   */
  public $ensure_unique;

  /**
   * Whether to generate a token.
   *
   * @var bool
   */
  public $generate_token;

}
