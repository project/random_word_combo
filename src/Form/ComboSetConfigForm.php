<?php

namespace Drupal\random_word_combo\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

class ComboSetConfigForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Label for the Random Word Combo."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => ['Drupal\random_word_combo\Entity\ComboSetConfig', 'load'],
        'source' => ['label'],
      ],
      '#disabled' => !$entity->isNew(),
      '#required' => TRUE,
    ];

    $form['left_words'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Left Words'),
      '#default_value' => $entity->left_words,
      '#description' => $this->t('Enter the words for the left side, separated by space.'),
      '#required' => TRUE,
    ];

    $form['right_words'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Right Words'),
      '#default_value' => $entity->right_words,
      '#description' => $this->t('Enter the words for the right side, separated by space.'),
      '#required' => TRUE,
    ];

    $form['ensure_unique'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ensure Unique'),
      '#return_value' => TRUE,
      '#default_value' => $entity->ensure_unique,
      '#description' => $this->t('Check if you want each combination to be unique. Note: This will use the database, and may impact performance. You also risk going out of possible tokens.'),
    ];

    $form['generate_token'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create tokens for set'),
      '#return_value' => TRUE,
      '#default_value' => $entity->generate_token,
      '#description' => $this->t('Check if you want to generate a token for the set, to use with token module. Note: if you have unique combos, you may run out of them, and further token usage will likely fail.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Preprocess the left and right words to remove multiple spaces.
    $leftWords = preg_replace('/\s+/', ' ', trim($entity->get('left_words')));
    $rightWords = preg_replace('/\s+/', ' ', trim($entity->get('right_words')));

    // Set the cleaned up values back on the entity.
    $entity->set('left_words', $leftWords);
    $entity->set('right_words', $rightWords);

    // Continue with the save process.
    $status = parent::save($form, $form_state);

    if ($status) {
      \Drupal::messenger()->addMessage($this->t('Saved the %label Random Word Combo.', ['%label' => $entity->label()]));
    } else {
      \Drupal::messenger()->addMessage($this->t('The %label Random Word Combo was not saved.', ['%label' => $entity->label()]), 'error');
    }

    $form_state->setRedirect('random_word_combo.combo_set_config_list');
  }
}
