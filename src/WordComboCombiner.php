<?php

namespace Drupal\random_word_combo;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a service for generating random word combinations.
 *
 * This service handles the creation of random combinations from specified
 * word lists and ensures their uniqueness based on the entity's requirements.
 */
class WordComboCombiner {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritDoc}
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger.
   * @param Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Database\Connection $database
   *   The database.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelFactoryInterface $loggerFactory,
    MessengerInterface $messenger,
    Connection $database

  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerFactory->get('random_word_combo');
    $this->messenger = $messenger;
    $this->database = $database;
  }

  /**
   * Creating combination per set.
   *
   * Combination can be unique or not, based on the setting of the entity.
   *
   * @param string $setID
   *   The id / machine name of the set.
   *
   * @param bool $dryRun
   *    If TRUE, the function will not save a combo to the database.
   *
   * @return string
   *   A generated combination, unique or not.
   */
  public function getRandomCombo($setId, $dryRun = FALSE) {

    $entity = $this->entityTypeManager->getStorage('random_word_combo')->load($setId);

    if (!$entity) {
      $this->logger->notice('No entities found with id @setId', ['@setId' => $setId]);
      return NULL;
    }

    $combo = $this->generateCombo($entity);

    // If we don't need a unique combo or have a dry run, return combo directly.
    if ($dryRun || !$entity->get('ensure_unique')) {
      return $combo;
    }

    // Set a maximum number of attempts to prevent infinite loops.
    $maxAttempts = 10;
    $attempt = 1; // First attempt is already made above.

    // Check if the first combo is unique (if needed) before entering the loop.
    $isUnique = $dryRun || !$entity->get('ensure_unique') || $this->isComboUnique($setId, $combo);

    while (!$isUnique && $attempt < $maxAttempts) {
      $combo = $this->generateCombo($entity);
      $isUnique = $this->isComboUnique($setId, $combo);
      $attempt++;
    }

    // Handle case where we run out of attempts without finding a unique combo.
    if (!$isUnique) {
      $message = $this->t('Failed to generate a unique combo for set @id after @max attempts. You might have run out of possible combinations.', ['@id' => $setId, '@max' => $maxAttempts]);
      $this->logger->warning($message);
      $this->messenger->addMessage($message);
      return NULL;
    }

    // If not a dry run and uniqueness is required, save the unique combo.
    if (!$dryRun && $entity->get('ensure_unique')) {
      $this->insertUniqueCombo($setId, $combo);
    }

    return $combo;

  }

  /**
   * Insert a unique combo into the database.
   *
   * @param string $setId
   *   The id / machine name of the set.
   * @param string $combo
   *   The generated combo.
   *
   * @return void
   */
  private function insertUniqueCombo($setId, $combo) {
    $this->database->insert('random_word_combo_combinations')
      ->fields([
        'combo_set_id' => $setId,
        'combination' => $combo,
      ])
      ->execute();
  }

  /**
   * Flush combos for a given set.
   *
   * @param string $setId The id / machine name of the set.
   * @return bool Returns TRUE if any rows were deleted, FALSE otherwise.
   */
  public function flushCombosForSet($setId) {
    try {
      // Executes the delete query and returns the number of rows affected.
      $num_deleted = $this->database->delete('random_word_combo_combinations')
                                    ->condition('combo_set_id', $setId)
                                    ->execute();

      // If $num_deleted is more than 0, rows were deleted.
      if ($num_deleted > 0) {
        $this->logger->info('Deleted @count combos for the set @setId.', ['@count' => $num_deleted, '@setId' => $setId]);
        return TRUE;
      } else {
        // No rows were deleted, which might not necessarily be an error.
        $this->logger->notice('No combos needed flushing for the set @setId.', ['@setId' => $setId]);
        return TRUE;
      }
    } catch (\Exception $e) {
      $this->logger->error('Failed to flush combos for the set @setId: @message', ['@setId' => $setId, '@message' => $e->getMessage()]);
      return FALSE;
    }
  }

  /**
   * Check if a generated combo is unique.
   *
   * @param string $setId
   * @param string $combo
   *
   * @return boolean
   */
  private function isComboUnique($setId, $combo) {
    $exists = $this->database->select('random_word_combo_combinations', 'c')
      ->fields('c', ['id'])
      ->condition('combo_set_id', $setId)
      ->condition('combination', $combo)
      ->execute()
      ->fetchField();
    return !$exists;
  }

  /**
   * Generate combo from left and right sides.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The generated combo.
   */
  private function generateCombo($entity) : String {
    // Split the text fields into arrays of words.
    $leftWords = explode(' ', $entity->get('left_words'));
    $rightWords = explode(' ', $entity->get('right_words'));

    // Randomly select one wowordsrom each side.
    $leftWord = $leftWords[array_rand($leftWords)];
    $rightWord = $rightWords[array_rand($rightWords)];

    // Combine the selected words.
    $combo = $leftWord . ' ' . $rightWord;
    return $combo;
  }

  /**
   * Provide example and statistics data per set
   *
   * @param string $setId
   *
   * @return array
   *   An example combo, number of generated combos, number of possible combos
   */
  public function getDescriptionValues($setId) {
    $entity = $this->entityTypeManager->getStorage('random_word_combo')->load($setId);

    if (!$entity) {
        return $this->t('Combo set not found.');
    }

    $exampleCombo = $this->getRandomCombo($setId, $dryRun = TRUE);
    $generatedCombos = $this->countExistingCombos($setId);
    $possibleCombos = $this->countPossibleCombos($entity);

    return [
      'exampleCombo' => $exampleCombo,
      'generatedCombos' => $generatedCombos,
      'possibleCombos' => $possibleCombos,
    ];

  }

  /**
   * Count possible combos
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *
   * @return integer
   *   Number of possible combos
   */
  private function countPossibleCombos($entity) : int {
    $leftCount = count(explode(' ', $entity->get('left_words')));
    $rightCount = count(explode(' ', $entity->get('right_words')));
    $possibleCombos = $leftCount * $rightCount;

    return (int) $possibleCombos;
  }

  /**
   * Count existing combos
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *
   * @return integer
   *   Number of esisting combos for set
   */
  private function countExistingCombos($setId) : int {
    $existingCombos = $this->database->select('random_word_combo_combinations', 'rwc')
                                      ->fields('rwc', ['id'])
                                      ->condition('combo_set_id', $setId)
                                      ->countQuery()
                                      ->execute()
                                      ->fetchField();

    return (int) $existingCombos;
  }

  /**
   * Get combo sets that should generate tokens
   *
   * @return array
   *   An array of combo set ids, for which tokens should be generated
   *
   */
  public function getSetsWithTokens() {
    $ids = $this->entityTypeManager
                ->getStorage('random_word_combo')
                ->getQuery()
                ->condition('generate_token', TRUE)
                ->execute();
    return $ids;
  }
}
